var http     = require('http');
var request  = require('request');
var _        = require('underscore');
var fs       = require('fs');
var mongoose = require('mongoose');

// conecta no meu banco
mongoose.connect('mongodb://localhost/givemytorrents');

// instancia um Schema
var Schema   = mongoose.Schema;

// Cria um schema ( quase como uma tabela ) de filmes, só vão ser inseridos filmes se forem obedecidos esses critérios
var movieSchema = new Schema({
  id: String,
  imdb: String,
  poster_big: String,
  poster_med: String,
  rating: Number,
  title: String,
  title_ptbr: String,
  trailer: String,
  year: Number,
  actors: String,
  description: String,
  directors: String,
  genres: [{ type: String}],
  items: [
    {
      file: String,
      id: String,
      language: String,
      quality: String,
      size_bytes: Number,
      subtitles: String,
      torrent_peers: Number,
      torrent_seeds: Number,
      torrent_url: String
    }
  ]
});

// cria uma colection filme e passa para a variável Movie
var Movie = mongoose.model('Filme', movieSchema);


http.createServer(function (req, res) {
   
    // informa que é uma página HTML
    res.writeHead(200, {'Content-Type': 'text/html'});
    
    /**
     * page : Número da página inicial da busca
     * totalSeires : informa o número total de séries / filmes buscados
     * templateHtml : é um template para exibição dos dados garimpados
     */
    var page        = 133;
    var totalSeries = 0;
    
    // lê o arquivo e transforma em template para ser usado depois.
    var templateHtml = fs.readFileSync('templateHtml.html').toString(); 
    
    function getFilmes(name){
        // faz um find por todos os titles com name x
        var aux   = Movie.find({ 'title' : new RegExp(name, 'i') });
        var count = 0;

        // executa o find
        aux.exec(function(err, movies){
            // percorre os resultados
            _.each(movies, function(filme, index){
                
                // usa o template carregado antes
                var div = _.template(templateHtml, { filme : filme });

                res.write(div);

                count++;
            });

            // escreve na tela a quantidade de resultados
            res.write('<h4 style="position: fixed; top: 0px; left: 0px; background: red;"> Encontrados: ' + count + '</h4>');

            // termina a requisição web
            res.end();
        });
    } // fim da função

    // Toda mágica de busca ocorre aqui 
    function novaConexao(page){
        
        // faz a busca pela API por x página
        request('http://api.torrentsapi.com/list?sort=seeds&page=' + page, function (error, response, body) {
            // recebe na tela qual página está buscando
            res.write('<h1>Page '+page+'</h1>');

            // se não ocorrer erro e for uma página válida, continua
            if (!error && response.statusCode == 200 ) {
                
                // se a resposta for == de uma listagem vazia, termina a requisição
                if(body == '{"MovieList":[]}'){
                    res.end('<h1>Total de Filmes: ' + totalSeries + '</h1>');
                }else{

                    var Filmes = JSON.parse(body);
                    var div; 
                    var Movie = mongoose.model('Filme', movieSchema);
                    
                    _.each(Filmes.MovieList, function(filme, index){
                        var m = new Movie;
                        m.id          = filme.id;
                        m.imdb        = filme.imdb;
                        m.poster_big  = filme.poster_big;
                        m.poster_med  = filme.poster_med;
                        m.rating      = filme.rating;
                        m.title       = filme.title;
                        m.title_ptbr  = "";
                        m.trailer     = filme.trailer;
                        m.year        = filme.year;
                        m.actors      = filme.actors;
                        m.description = filme.description;
                        m.directors   = filme.directors;
                        m.genres      = filme.genres;
                        m.items       = filme.items;
                       
                        m.save(function(err, Movie){
                            if(err){
                                console.log(err);
                                res.write(err.toString());
                            }else{
                                console.log(Movie);
                            }
                        });
                       
                        totalSeries++;
                        // div = _.template(templateHtml, { filme : filme });
                        // res.write(div);
                    });                     
            
                    // manda para a próxima página
                    page++;
                    novaConexao(page);
                }
            }  
            else{
                res.write(error.toString());
            }  
        });
    } // fim da função

    // inicia o script que busca e insere todos os filmes 
    // novaConexao(page);
    
    // inicia a function que busca por nome os filmes localmente
    getFilmes('sex');
                   

}).listen(1337, '127.0.0.1'); // inicia o servidor

console.log('Server running at http://127.0.0.1:1337/');