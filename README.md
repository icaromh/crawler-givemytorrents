# Crawler para alimentação da API
---

## Baixando e iniciando o projeto

### Passos: 

* Instale o [node.js](http://nodejs.org/)

* Baixe o projeto

* Navegue até a pasta do projeto e instale as dependências com `npm install`

* Inicie o servidor com `node server.js`


### Opcional:

* Instalar [nodemon](http://nodemon.io/) globalmente com `npm install -g nodemon`

* Iniciar `nodemon server.js`